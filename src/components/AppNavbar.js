// import Navbar from "react-bootstrap/Navbar";
// import Nav from "react-bootstrap/Nav";
import { Link } from "react-router-dom";
import {useState, useContext} from "react";
import UserContext from "../UserContext"


import {Navbar, Nav, Container, NavDropdown} from "react-bootstrap";

export default function AppNavbar(){

    const {user} = useContext(UserContext);

    // State hook to store the information stored in the login page.
    // const [user, setUser] = useState(localStorage.getItem("email"));
    // console.log(user);

    return (
        <Navbar bg="primary" expand="lg">
      <Container>
        <Navbar.Brand as={Link} to="/">Zuitt</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
            {/* className is use instead of "class", to specify a CSS classes */}
          <Nav className="ms-auto" defaultActiveKey="/">
            <Nav.Link as={Link} to="/" eventKey="/">Home</Nav.Link>
            <Nav.Link as={Link} to="/courses" eventKey="/courses">Course</Nav.Link>
            {
              /* 
              user.email is causing an error when the local storage is cleared because the initial state of user is change to null and we can't perform a dot notation to a non-object datatype.
              */
            }
            {
              (user.id !== null)
              ?
                <Nav.Link as={Link} to="/logout" eventKey="/logout">Logout</Nav.Link>
              :
                <>
                  <Nav.Link as={Link} to="/login" eventKey="/login">Login</Nav.Link>
                  <Nav.Link as={Link} to="/register" eventKey="/register">Register</Nav.Link>
                </>
            }
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
    )
}